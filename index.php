$app->get("/calcular-frete-:cep", function($cep){

    $sql = new Sql();

    $result = $sql->select("CALL sp_carrinhos_get('".session_id()."')");

    $carrinho = $result[0];

    $sql = new Sql();

   $produtos = $sql->select("CALL sp_carrinhosprodutosfrete_list(".$carrinho['id_car'].")");

    $peso = 0;
    $comprimento = 0;
    $altura = 0;
    $largura = 0;
    $valor = 0;

    foreach ($produtos as $produto) {
        $peso =+ $produto['peso'];
        $comprimento =+ $produto['comprimento'];
        $altura =+ $produto['altura'];
        $largura =+ $produto['largura'];
        $valor =+ $produto['preco'];
    }

    $cep = trim(str_replace('-', '', $cep));

    require("inc/php-calculo-frete-master/frete.php");

    $frete = new Frete();
    $frete =$frete->calcularFrete(
        $cepOrigem = "78060000",
        $cepDestino = $cep,
        $peso = $peso,
        $comprimento = $comprimento,
        $altura = $altura,
        $largura = $largura,
        $tipoDeEntrega = "40010");

    $sql = new Sql();
        
    $url = "UPDATE tb_carrinhos SET frete_car = '";
    $url .= $frete['valor'];
    $url .= "', cep_car = '";
    $url .= $cep;
    $url .= "', prazo_car = '";
    $url .= $frete['prazo'];
    $url .= "' WHERE id_car = ";
    $url .= $carrinho['id_car'];
    $url .= ";";

    $sql->query($url);
    
    echo json_encode(array(
        'success'=>true
    ));
    
});